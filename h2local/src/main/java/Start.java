import org.h2.tools.Server;

import java.util.Scanner;

public class Start {
    public static void main(String[] args) throws Exception {
        Server tcpServer = Server.createTcpServer("-tcpAllowOthers", "-tcpPort", "8090");
        tcpServer.start();

        Server webServer = Server.createWebServer("-web", "-webAllowOthers", "-webPort", "8091");
        webServer.start();

        System.out.println("TcpUrl " + tcpServer.getURL());
        System.out.println("WebUrl " + webServer.getURL());
        System.out.println("Press enter to exit");
        Scanner scan = new Scanner(System.in);
        String line = scan.nextLine();
        System.out.println("got " + line);

        Server.shutdownTcpServer(tcpServer.getURL(), null, true, true);
        webServer.shutdown();
    }
}
