package no.utgdev.director;

import no.utgdev.director.config.ApplicationConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@Import(ApplicationConfig.class)
@SpringBootApplication
public class StartLocal {
    public static void main(String[] args) {
        SpringApplication.run(Main.class);
    }
}
