package no.utgdev.director;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import io.vavr.control.Option;
import lombok.SneakyThrows;
import no.utgdev.director.core.TypedField;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.function.Function;

public class SerializerUtils {
    public final static ObjectMapper mapper = new ObjectMapper();

    static {
        mapper.registerModule(new ParameterNamesModule());
        mapper.registerModule(new Jdk8Module());
        mapper.registerModule(new JavaTimeModule());
    }

    @SneakyThrows
    public static String serialize(Object object) {
        return mapper.writeValueAsString(object);
    }

    @SneakyThrows
    public static TypedField deserialize(String data) {
        return mapper.readValue(data, TypedField.class);
    }

    public static <S, T> T nullAllowed(S s, Function<S, T> fn) {
        return Option.of(s)
                .map(fn)
                .getOrElse((T)null);
    }
}
