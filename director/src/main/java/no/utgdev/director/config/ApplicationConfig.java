package no.utgdev.director.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import no.utgdev.director.SerializerUtils;
import no.utgdev.director.core.Core;
import no.utgdev.director.core.TaskDAO;
import no.utgdev.director.service.ActuatorService;
import no.utgdev.director.test.TestIntegration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableScheduling
@EnableAsync
@EnableTransactionManagement
@Import({DbConfig.class})
public class ApplicationConfig {
    @Bean
    public TestIntegration testIntegration() {
        return new TestIntegration();
    }

    @Bean
    public ActuatorService actuatorService() {
        return new ActuatorService();
    }

    @Bean
    public ObjectMapper objectMapper() {
        return SerializerUtils.mapper;
    }

    @Bean
    public TaskDAO taskDAO() {
        return new TaskDAO();
    }

    @Bean
    public ThreadPoolTaskScheduler threadPoolTaskScheduler() {
        ThreadPoolTaskScheduler threadPoolTaskScheduler = new ThreadPoolTaskScheduler();
        threadPoolTaskScheduler.setPoolSize(4);
        threadPoolTaskScheduler.setThreadNamePrefix("ThreadPoolTaskScheduler");
        return threadPoolTaskScheduler;
    }

    @Bean
    public Core core() {
        return new Core();
    }
}
