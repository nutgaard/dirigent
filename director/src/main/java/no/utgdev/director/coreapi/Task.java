package no.utgdev.director.coreapi;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Wither;
import no.utgdev.director.SerializerUtils;
import no.utgdev.director.core.TypedField;
import no.utgdev.director.domain.tables.records.TaskRecord;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Wither
public class Task<DATA, RESULT> {
    String id;
    TaskType type;
    Status status;
    int attempts;
    LocalDateTime created;
    LocalDateTime lastAttempt;
    LocalDateTime nextAttempt;
    int step;
    TypedField<DATA> data;
    TypedField<RESULT> result;
    String error;

    @SuppressWarnings("unchecked")
    public static <DATA, RESULT> Task<DATA, RESULT> fromRecord(TaskRecord rec) {
        return new Task<DATA, RESULT>(
                rec.getId(),
                TaskType.of(rec.getType()),
                Status.valueOf(rec.getStatus()),
                rec.getAttempts().intValue(),
                rec.getCreated().toLocalDateTime(),
                SerializerUtils.nullAllowed(rec.getLastattempt(), Timestamp::toLocalDateTime),
                SerializerUtils.nullAllowed(rec.getNextattempt(), Timestamp::toLocalDateTime),
                rec.getStep().intValue(),
                SerializerUtils.deserialize(rec.getData()),
                SerializerUtils.deserialize(rec.getResult()),
                rec.getError()
        );
    }

    public TaskRecord toRecord() {
        TaskRecord res = new TaskRecord();
        res.setId(id);
        res.setType(type.getType());
        res.setAttempts(BigDecimal.valueOf(attempts));

        if (status == null) {
            status = Status.PENDING;
        }
        res.setStatus(status.toString());

        if (lastAttempt != null) {
            res.setLastattempt(Timestamp.valueOf(lastAttempt));
        }
        if (nextAttempt != null) {
            res.setLastattempt(Timestamp.valueOf(nextAttempt));
        }

        res.setStep(BigDecimal.valueOf(step));
        res.setData(SerializerUtils.serialize(data));
        res.setResult(SerializerUtils.serialize(data));
        res.setError(error);

        return res;
    }
}
