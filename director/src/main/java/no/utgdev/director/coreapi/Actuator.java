package no.utgdev.director.coreapi;

import io.vavr.control.Try;

public interface Actuator<DATA, RESULT> {
    public Try<RESULT> handle(String id, DATA data);
}
