package no.utgdev.director.coreapi;

public enum Status {
    PENDING, WORKING, OK, FAILED
}
