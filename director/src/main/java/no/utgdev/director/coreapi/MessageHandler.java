package no.utgdev.director.coreapi;

import io.vavr.collection.List;

public interface MessageHandler {
    List<Task> handle(Message message);
}
