package no.utgdev.director.test;

import io.vavr.control.Either;
import io.vavr.control.Try;

import java.util.function.Function;

public class Test {
    public static void main(String[] args) {
        Either<String, Integer> lefty = Either.left("failure :(");

        Integer orElseThrow = lefty.getOrElseThrow((Function<String, RuntimeException>) RuntimeException::new);

        lefty
                .toTry()
                .onSuccess(res -> System.out.println("yay"))
                .onFailure(error -> System.out.print("buuu"));
    }
}
