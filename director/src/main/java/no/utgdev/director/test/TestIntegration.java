package no.utgdev.director.test;

import io.vavr.collection.HashMap;
import io.vavr.collection.List;
import io.vavr.control.Either;
import io.vavr.control.Option;
import io.vavr.control.Try;
import no.utgdev.director.core.Core;
import no.utgdev.director.core.TaskDefinitionRepository;
import no.utgdev.director.core.TypedField;
import no.utgdev.director.coreapi.*;
import no.utgdev.director.service.ActuatorService;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.Map;
import java.util.function.Function;

public class TestIntegration implements MessageHandler, Actuator<TestData, String> {
    private final TaskType TYPE = TaskType.of("TEST_TYPE");

    @Inject
    Core core;
    @Inject
    ActuatorService service;

    @PostConstruct
    public void register() {
        core.registerHandler(this);
        core.registerActuator(TYPE, this);
    }

    @Override
    public List<Task> handle(Message msg) {
        if (msg instanceof TestMessage) {
            TestMessage message = (TestMessage) msg;

            return List.of(
                    new Task<TestData, String>()
                            .withId(message.getId() + "-" + "01")
                            .withType(TYPE)
                            .withData(new TypedField<>(new TestData("activity_1", message.getPerson(), message.isShouldAct()))),
                    new Task<TestData, String>()
                            .withId(message.getId() + "-" + "02")
                            .withType(TYPE)
                            .withData(new TypedField<>(new TestData("dialog_1", message.getPerson(), message.isShouldAct()))),
                    new Task<TestData, String>()
                            .withId(message.getId() + "-" + "03")
                            .withType(TYPE)
                            .withData(new TypedField<>(new TestData("dialog_2", message.getPerson(), message.isShouldAct())))
            );
        }
        return List.empty();
    }

    @Override
    public Try<String> handle(String id, TestData data) {
        System.out.println("Handling " + data.toString());
        Map map = TaskDefinitionRepository.get(data.contentname, java.util.Map.class).getOrElse(new java.util.HashMap());
        map.put("id", id);

        return service.publish(map);
    }
}
