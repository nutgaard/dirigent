package no.utgdev.director.test;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Value;
import lombok.experimental.Wither;
import no.utgdev.director.coreapi.Message;

@Data
@Wither
@NoArgsConstructor
@AllArgsConstructor
public class TestMessage implements Message {
    String id;
    String person;
    boolean shouldAct;
}
