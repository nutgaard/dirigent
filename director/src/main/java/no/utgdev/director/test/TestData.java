package no.utgdev.director.test;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Wither;

@Data
@Wither
@NoArgsConstructor
@AllArgsConstructor
public class TestData {
    String contentname;
    String person;
    boolean shouldAct;
}
