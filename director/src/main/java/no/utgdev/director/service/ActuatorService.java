package no.utgdev.director.service;

import io.vavr.control.Try;
import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Map;

@Slf4j
public class ActuatorService {
    private static Client client = ClientBuilder.newClient();
    private static WebTarget target = client.target("http://localhost:8093/api/receiver");

    public Try<String> publish(Map<String, String> data) {
        log.info("Got data: " + data.toString());
        Response post = target
                .request()
                .post(Entity.entity(data, MediaType.APPLICATION_JSON));

        if (post.getStatus() == 200) {
            return Try.success(post.readEntity(String.class));
        } else {
            log.error("Failed in actuator", data);
            throw new RuntimeException("Failed in actuator");
        }
    }
}
