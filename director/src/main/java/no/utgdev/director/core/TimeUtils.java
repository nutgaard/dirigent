package no.utgdev.director.core;

import java.time.LocalDateTime;

public class TimeUtils {
    public static LocalDateTime exponentialBackoff(int step, LocalDateTime now) {
        long secondsToWait = (long)Math.pow(2, step);
        return now.plusSeconds(secondsToWait);
    }
}
