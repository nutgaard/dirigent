package no.utgdev.director.core;

import io.vavr.collection.HashMap;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import io.vavr.control.Try;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import no.utgdev.director.coreapi.*;
import no.utgdev.director.domain.Tables;
import no.utgdev.director.domain.tables.records.TaskRecord;
import org.jooq.Condition;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.function.Supplier;

@Slf4j
public class Core {
    private List<MessageHandler> handlers = List.empty();
    private Map<TaskType, Actuator> actuators = HashMap.empty();
    private TransactionTemplate transactionTemplate;
    private ForkJoinPool actuatorThreadPool = new ForkJoinPool(8);

    @Inject
    private ThreadPoolTaskScheduler scheduler;
    @Inject
    private TaskDAO taskDAO;
    @Inject
    private PlatformTransactionManager transactionManager;

    @PostConstruct
    public void setup() {
        this.transactionTemplate = new TransactionTemplate(transactionManager);
    }

    public void registerHandler(MessageHandler handler) {
        this.handlers = handlers.append(handler);
    }

    public void registerActuator(TaskType type, Actuator actuator) {
        this.actuators = this.actuators.put(type, actuator);
    }

    @Transactional
    public boolean submit(Message message) {
        try {
            List<Task> tasks = handlers
                    .flatMap((handler) -> handler.handle(message))
                    .map((task) -> task.withStatus(Status.PENDING));

            taskDAO.insert(tasks);

            System.out.println("Active count: " + scheduler.getPoolSize() + " " + scheduler.getActiveCount());
            scheduler.execute(this::runActuators);

            return true;
        } catch (Exception e) {
            log.error("Error while handling message", e);
            throw new RuntimeException(e);
        }
    }

    @Transactional(propagation = Propagation.MANDATORY)
    public boolean submitInTransaction(Message message) {
        return submit(message);
    }

    @SuppressWarnings("unchecked")
    @Scheduled(fixedDelay = 60000)
    @SneakyThrows
    public void runActuators() {
        final List<Task> tasks = prepTasks();
            tasks.forEach((task) -> actuatorThreadPool.submit(() -> this.tryActuators(task)));
//        actuatorThreadPool.submit(() -> {
//            System.out.println("Picked up " + tasks.size() + "tasks...");
//            tasks.toJavaParallelStream().forEach(this::tryActuators);
//        });
    }

    private synchronized List<Task> prepTasks() {
        return inTransaction(() -> {
            List<Task> tasks = taskDAO.fetchTasksReadyForExecution();
            taskDAO.setStatus(tasks, Status.WORKING);
            return tasks;
        });
    }

    @SuppressWarnings("unchecked")
    private void tryActuators(Task<?, ?> task) {
        this.actuators.get(task.getType()).forEach((actuator -> tryActuator(actuator, task)));
    }

    private <DATA, RESULT> void tryActuator(Actuator<DATA, RESULT> actuator, Task<DATA, RESULT> task) {
        inTransaction(() -> {
            Try.of(() -> actuator.handle(task.getId(), task.getData().element))
                    .flatMap(Function.identity())
                    .onFailure((throwable) -> {
                        Task withError = task.withError(throwable.toString());
                        taskDAO.setStatus(withError, Status.FAILED);
                    })
                    .onSuccess((result) -> {
                        Task withResult = task.withResult(new TypedField<>(result));
                        taskDAO.setStatus(withResult, Status.OK);
                    });
        });
    }

    private void inTransaction(Runnable runnable) {
        transactionTemplate.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                runnable.run();
            }
        });
    }

    private <T> T inTransaction(Supplier<T> supplier) {
        return transactionTemplate.execute(status -> supplier.get());
    }
}
