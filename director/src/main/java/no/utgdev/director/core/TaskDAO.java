package no.utgdev.director.core;

import io.vavr.collection.List;
import no.utgdev.director.SerializerUtils;
import no.utgdev.director.coreapi.Status;
import no.utgdev.director.coreapi.Task;
import no.utgdev.director.domain.Tables;
import no.utgdev.director.domain.tables.records.TaskRecord;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.UpdatableRecord;

import javax.inject.Inject;

import java.math.BigDecimal;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDateTime;

import static no.utgdev.director.domain.tables.Task.TASK;

public class TaskDAO {
    @Inject
    DSLContext dsl;

    public int insert(Task task) {
        return insert(List.of(task))[0];
    }

    public int[] insert(List<Task> tasks) {
        return dsl.batch(
                tasks
                        .map((task) -> dsl.insertInto(TASK).set(task.toRecord()))
                        .toJavaList()
        ).execute();
    }

    public <DATA, RESULT> Task<DATA, RESULT> get(String taskId) {
        return dsl
                .selectFrom(TASK)
                .where(TASK.ID.equal(taskId))
                .fetchOne(Task::fromRecord);
    }

    public <DATA, RESULT> List<Task<DATA, RESULT>> getAll(String type) {
        return List.ofAll(dsl
                .selectFrom(TASK)
                .where(TASK.TYPE.eq(type))
                .fetch()
                .map(Task::fromRecord));
    }

    public List<Task> getAll(Condition... conditions) {
        return List.ofAll(dsl
                .selectFrom(TASK)
                .where(conditions)
                .limit(1000)
                .fetch()
                .map(Task::fromRecord));
    }

    public int setStatus(List<Task> tasks, Status status) {
        List<String> ids = tasks.map(Task::getId);
        return dsl.update(TASK)
                .set(TASK.STATUS, status.toString())
                .where(TASK.ID.in(ids.toJavaList()))
                .execute();
    }

    public int setStatus(Task task, Status status) {
        LocalDateTime now = LocalDateTime.now();
        TaskRecord record = new TaskRecord();
        record.setLastattempt(Timestamp.valueOf(now));
        record.setStatus(status.toString());

        if (status == Status.FAILED) {
            LocalDateTime nextRetry = TimeUtils.exponentialBackoff(task.getAttempts(), now);
            record.setNextattempt(Timestamp.valueOf(nextRetry));
            record.setAttempts(BigDecimal.valueOf(task.getAttempts()).add(BigDecimal.ONE));
            record.setError(task.getError());
        }

        if (status != Status.OK) {
            record.setResult(SerializerUtils.serialize(task.getResult()));
        }

        return this.update(record, TASK.ID.equal(task.getId()));
    }

    public int update(TaskRecord task, Condition... conditions) {
        return dsl.update(TASK)
                .set(task)
                .where(conditions)
                .execute();
    }

    public List<Task> fetchTasksReadyForExecution() {
        Condition waitingStatus = Tables.TASK.STATUS.eq(Status.FAILED.toString()).or(Tables.TASK.STATUS.eq(Status.PENDING.toString()));
        Condition waitedLongEnough = Tables.TASK.NEXTATTEMPT.le(Timestamp.valueOf(LocalDateTime.now()));
        return getAll(waitingStatus, waitedLongEnough);
    }
}
