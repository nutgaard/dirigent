package no.utgdev.director.core;

import io.vavr.Tuple;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import io.vavr.control.Either;
import io.vavr.control.Option;
import lombok.extern.slf4j.Slf4j;
import no.utgdev.director.SerializerUtils;

import java.io.File;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.function.Function;

@Slf4j
public class TaskDefinitionRepository {

    private static Map<String, String> taskcontent;

    public static <T> Option<T> get(String name, Class<T> type) {
        if (taskcontent == null) {
            taskcontent = readAllFiles();
        }

        return taskcontent
                .get(name)
                .onEmpty(() -> log.warn("Requested '" + name + "', but taskdefinition does not exist."))
                .flatMap((content) -> readvalue(content, type).toOption());
    }

    private static <T> Either<Exception, T> readvalue(String content, Class<T> type) {
        try {
            return Either.right(SerializerUtils.mapper.readValue(content, type));
        } catch (Exception e) {
            log.error("Parsing content", e);
            return Either.left(e);
        }
    }

    private static Map<String, String> readAllFiles() {
        return Option.of(TaskDefinitionRepository.class.getClassLoader().getResource("data"))
                .map(URL::getFile)
                .map(File::new)
                .map(TaskDefinitionRepository::listFiles)
                .getOrElse(List.empty())
                .map((file) -> Tuple.of(findName(file), readFile(file).getOrElse((String) null)))
                .toMap(Function.identity());
    }

    private static String findName(File file) {
        String filenameWithExtension = file.getName();
        int extensionIndex = filenameWithExtension.lastIndexOf(".");

        return filenameWithExtension.substring(0, extensionIndex);
    }

    private static Either<Exception, String> readFile(File file) {
        try {
            return Either.right(new String(Files.readAllBytes(Paths.get(file.getAbsolutePath()))));
        } catch (Exception e) {
            log.error("Reading file", e);
            return Either.left(e);
        }
    }

    private static List<File> listFiles(File directory) {
        return Option.of(directory.listFiles())
                .map(List::of)
                .getOrElse(List.empty())
                .flatMap((File file) -> {
                    if (file.isFile()) {
                        return List.of(file);
                    } else {
                        return listFiles(directory);
                    }
                });
    }
}
