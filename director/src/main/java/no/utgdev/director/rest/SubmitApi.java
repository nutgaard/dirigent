package no.utgdev.director.rest;

import no.utgdev.director.core.Core;
import no.utgdev.director.domain.Tables;
import no.utgdev.director.test.TestMessage;
import org.jooq.DSLContext;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/submit")
@Consumes(MediaType.APPLICATION_JSON)
@Controller
public class SubmitApi {

    @Inject
    Core core;

    @Inject
    DSLContext dsl;

    @POST
    public Response submit(TestMessage msg) {
        boolean b = core.submit(msg);
        return Response.ok(b).build();
    }

    @POST
    @Path("/db")
    @Transactional
    public Response submitWithDb(TestMessage msg) {
        boolean tasksCreated = core.submitInTransaction(msg);

        if (tasksCreated) {
            dsl.insertInto(Tables.TEST_CONSTRAINT)
                    .set(Tables.TEST_CONSTRAINT.ID, msg.getPerson())
                    .execute();
        }

        return Response.ok(tasksCreated).build();
    }
}
