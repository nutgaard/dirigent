package no.utgdev.director.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("/test")
public class TestApi {

    @GET
    public String testApi() {
        return "This is fromRecord an api";
    }
}
