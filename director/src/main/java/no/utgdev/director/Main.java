package no.utgdev.director;

import no.utgdev.director.config.ApplicationConfig;
import no.utgdev.director.config.DbConfig;
import no.utgdev.director.core.Core;
import no.utgdev.director.test.TestMessage;
import org.flywaydb.core.Flyway;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Import;
import org.springframework.util.SocketUtils;

import javax.sql.DataSource;
import java.util.Properties;

@Import(ApplicationConfig.class)
@SpringBootApplication
public class Main {
    public static void main(String[] args) {
        Properties properties = new Properties();
        properties.setProperty("server.port", "8092");
        properties.setProperty("spring.datasource.url", "jdbc:h2:tcp://localhost:8090/./director");
        properties.setProperty("spring.datasource.username", "sa");

        Flyway flyway = new Flyway();
        flyway.setDataSource(Main.dataSource(properties));
        flyway.migrate();



        ConfigurableApplicationContext context = new SpringApplicationBuilder()
                .properties(properties)
                .sources(Main.class)
                .run(args);

        Core core = context.getBean(Core.class);

        TestMessage msg = new TestMessage("1350", "12345678901", true);
//        boolean first = core.submit(msg);
    }

    private static DataSource dataSource(Properties properties) {
        return DataSourceBuilder
                .create()
                .url(properties.getProperty("spring.datasource.url"))
                .driverClassName(properties.getProperty("spring.datasource.driver-class-name"))
                .username(properties.getProperty("spring.datasource.username"))
                .build();
    }
}
