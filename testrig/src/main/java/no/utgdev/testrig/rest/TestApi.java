package no.utgdev.testrig.rest;

import org.springframework.stereotype.Controller;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("/test")
@Controller
public class TestApi {

    @GET
    public String testApi() {
        return "This is fromRecord an api";
    }
}
