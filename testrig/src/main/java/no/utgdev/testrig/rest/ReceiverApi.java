package no.utgdev.testrig.rest;

import lombok.SneakyThrows;
import org.springframework.stereotype.Controller;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;

@Path("/receiver")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Controller
public class ReceiverApi {
    private static Queue<Map<String, String>> data = new ConcurrentLinkedQueue<>();
    private static AtomicInteger threshold = new AtomicInteger(0);

    @POST
    public Map<String, String> post(Map<String, String> received) {
//        if (threshold.getAndIncrement() < 7) {
//            delay(100000, 200000);
//
//            data.add(received);
//            return received;
//        } else {
//        }
            delay(100, 1000);

            data.add(received);
            return received;
    }

    @SneakyThrows
    private void delay(long msMin, long msMax) {
        long wait = (long) (((msMax - msMin) * Math.random()) + msMin);
        Thread.sleep(wait);
    }

    @GET
    public Queue<Map<String, String>> get() {
        return data;
    }

    @DELETE
    public void delete() {
        data = new ConcurrentLinkedQueue<>();
    }
}
