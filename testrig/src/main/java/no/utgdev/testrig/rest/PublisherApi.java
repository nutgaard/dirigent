package no.utgdev.testrig.rest;

import org.springframework.stereotype.Controller;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Map;

@Path("/publish")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Controller
public class PublisherApi {

    @Inject
    Client client;

    @POST
    public Map post(Map<String, String> data) {
        WebTarget target = client.target("http://localhost:8092/api/submit");
        Response post = target.request().post(Entity.entity(data, MediaType.APPLICATION_JSON));

        return post.readEntity(Map.class);
    }
}
