package no.utgdev.testrig;

import no.utgdev.testrig.config.ApplicationConfig;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Import;

import java.util.Properties;

@Import(ApplicationConfig.class)
@SpringBootApplication
public class Main {
    public static void main(String[] args) {
        Properties properties = new Properties();
        properties.setProperty("server.port", "8093");

        new SpringApplicationBuilder()
                .properties(properties)
                .sources(Main.class)
                .run(args);
    }
}
