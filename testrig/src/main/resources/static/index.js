const form = document.querySelector('.publish');
const batchCount = document.querySelector('.count');
const batchTime = document.querySelector('.time');
const batchBtn = document.querySelector('.batch');
const content = document.querySelector('.content');
const refresh = document.querySelector('.refresh');
const clear = document.querySelector('.clear');

function chunk(array, size) {
    return array.reduce((ar, it, i) => {
        const ix = Math.floor(i / size);

        if (!ar[ix]) {
            ar[ix] = [];
        }

        ar[ix].push(it);

        return ar;
    }, []);
}

function rndString() {
    return Math.random().toString(16).slice(2);
}

function rndBoolean() {
    return Math.random() > 0.5;
}

function publish(data) {
    return fetch('http://localhost:8092/api/submit/db', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });
}

function refreshMetrics(){
    fetch('http://localhost:8093/api/receiver')
        .then((resp) => resp.json())
        .then((json) => {
            const set = new Set();
            json.forEach((r) => set.add(r.id));

            content.innerHTML = '';
            const child = document.createElement('p');
            child.innerText = `Received ${json.length} elements, with ${set.size} unique ids`;
            content.appendChild(child);

            json.forEach((r) => {
                const child = document.createElement('p');
                child.innerText = JSON.stringify(r);
                content.appendChild(child);
            })
        })
}

function clearMetrics() {
    fetch('http://localhost:8093/api/receiver', {
        method: 'DELETE'
    });
}

function findValues(form) {
    const values = Array.from(form.querySelectorAll('input'))
        .filter((input) => input.type !== 'submit')
        .map((input) => {
            const value = input.type !== 'checkbox' ? input.value : input.checked;
            return {[input.name]: value};
        })
        .reduce((a, b) => ({...a, ...b}), {});

    if (!values.id || values.id.length === 0) {
        values.id = rndString();
    }

    return publish(values);
}

form.addEventListener('submit', (e) => {
    e.preventDefault();
    const values = findValues(form);
    console.log('values', values); // tslint:disable-line
});

batchBtn.addEventListener('click', (e) => {
    e.preventDefault();
    const batchSize = parseInt(batchCount.options[batchCount.selectedIndex].value);
    const batchTimes = parseInt(batchTime.options[batchTime.selectedIndex].value) * 1000;
    const batch = new Array(batchSize)
        .fill(0)
        .map(() => ({
            id: rndString(),
            person: rndString(),
            shouldAct: rndBoolean()
        }));

    if (batchTimes === 0) {
        batch.forEach(publish);
    } else {
        const timeslice = Math.round(batchTimes / batch.length);

        batch.forEach((job, index) => {
            console.log('Scheduled job', job, 'in', index * timeslice, 'ms'); // tslint:disable-line
            setTimeout(() => publish(job), index * timeslice);
        })
    }
});

refresh.addEventListener('click', refreshMetrics);
clear.addEventListener('click', clearMetrics);
refreshMetrics();

setInterval(refreshMetrics, 3000);